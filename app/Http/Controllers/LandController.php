<?php

namespace App\Http\Controllers;

use App\Models\Land;
use Illuminate\Http\Request;

class LandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()

    {
        $lands = Land::all();
        return view('pages.users.index', ["lands" => $lands]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){

        return view('pages.users.create');
    }

      //

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store (Request $request){
        $request->validate([
            "libelle"=>"required",
            "description" => "required",
            "code_indicatif" => "required|code_indicatif|unique:lands,code_indicatif",
            "continent"=> "required",
            "Population" => "required",
            "capitale" => "required",
            "monnaie" => "required",
            "Langue" => "required",
            "Superficie" => "required",
            "est_laique" => "required",

        ]);

        Land::create([

            "libelle" => $request->get("libelle"),
            "description" => $request->get("description"),
            "code_indicatif" =>$request->get("code_indicatif"),
            "continent"=>$request->get("continent"),
            "Population"=> $request->get("Population"),
            "capitale" => $request->get("capitale"),
            "monnaie" => $request->get("monnaie"),
            "Langue" => $request->get(" Langue"),
            "Superficie" => $request->get("Superficie"),
            "est_laique" => $request->get("est laique"),

        ]);

        return redirect()->route('users.index')->with('flash_message','Land added');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lands = Land::findOrFail($id);
        return view("pages.users.show", ["lands" => $lands]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lands = Land::findOrFail($id);
        return view("pages.users.edit", ["lands" => $lands]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Land $lands )
    {

        $request->validate([
            "libelle" => 'required',
            "description" => 'required',
            "code_indicatif" => 'required',
            "Population" => "required",
            "capitale" => "required",
            "monnaie" => "required",
            "Langue" => "required",
            "Superficie" => "required",
            "est_laique" => "required",
        ]);

        $lands->update($request->all());

        return redirect()->route('users.index')
                        ->with('success',' mise à jour de la liste');
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Land $lands)
    {
        $lands->delete();

        return redirect()->route('users.index')
                        ->with('success','Pays supprimé avec succès');
    }

}
