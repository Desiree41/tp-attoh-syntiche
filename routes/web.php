<?php

use Illuminate\Support\Facades\Route;
use App\Models\Land;
use App\Http\Controllers\LandController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
   // return view('welcome');
//});
//Route pour retourner la vue main
//Route::view('/', 'layouts.main');
Route::view('/', 'pages.home')->name('main');
Route::get('/users', [LandController::class, "index"])->name('users.index');
Route::get('/users/create', [LandController::class, "create"])->name('users.create');
Route::get('/users/{id}', [LandController::class, "show"])->name('users.show');
Route::get('/users/{id}/edit', [LandController::class, "edit"])->name('users.edit');
Route::post('/users', [LandController::class,"store"])->name('users.store');
//Route::post('/users/{id}/update', [LandController::class, "update"])->name('users.update');
//Route::post('/users', [LandController::class, "delete"])->name('users.delete');


