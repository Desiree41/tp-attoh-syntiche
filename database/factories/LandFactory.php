<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Factory as Faker;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Land>
 */
class LandFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [

            "libelle" =>$this->faker->unique()->country(),
            "description" =>$this->faker->sentence(),
           'code_indicatif' =>$this->faker->numerify(' CC- ####'),
            "Continent" =>$this->faker->countryCode(),
            "Population" =>$this->faker->sentence(),
            "capitale" =>$this->faker->name(),
            "monnaie" =>$this->faker->countryISOAlpha3(),
            "Langue" =>$this->faker->countryISOAlpha3,
            'Superficie' =>$this->faker->numerify('########'),
            "Continent" =>$this->faker->state(),
            "est_Laique" => $this ->faker->boolean(),

            //
        ];
    }
}
