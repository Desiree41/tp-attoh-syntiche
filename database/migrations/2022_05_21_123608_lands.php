<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('lands', function (Blueprint $table) {
            $table->id();
            $table->string('libelle')->country();
            $table->string('description')->sentence();
            $table->string('code_indicatif')->unique();
            $table->string('Continent');
            $table->string('Population');
            $table->string('capitale');
            $table->string('monnaie');
            $table->string('Langue');
            $table->string('Superficie');
            $table->boolean('est_laique');
            $table->timestamps();
         });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
