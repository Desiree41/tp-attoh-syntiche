@extends('layouts.main')
@section('content')
<center>
<div>
    <h1 class="pl-5 pr-5" style="color: green"><center>Information d'un pays</center></h1>
    <p><center> Vouliez vous avoir plus d'informations sur ce pays?</center></p>
</div>
<div class="col-lg-12">
    <div class="ibox ">
        <div class="ibox-title">
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-wrench"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="#" class="dropdown-item">Config option 1</a>
                    </li>
                    <li><a href="#" class="dropdown-item">Config option 2</a>
                    </li>
                </ul>
                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="ibox-content">

            <div class="form-group row"><label class="col-lg-2 col-form-label" style=color:blue><center>Libelle:</center></label>
                <div class="col-lg-10"><input type="text" class="form-control" name="nom"
                        value="{{ $lands->libelle }}" disabled="disabled">
                </div>
            </div>
            <div class="form-group row"><label class="col-lg-2 col-form-label" style=color:red><center>Description:</center></label>
                <div class="col-lg-10"><input type="text" class="form-control" name="text"
                        value="{{ $lands->description}}" disabled="disabled">
                </div>
            </div>

            <div class="form-group row"><label class="col-lg-2 col-form-label" style=color:yellow><center>Code Indicatif:</center></label>
                <div class="col-lg-10"><input type="text" class="form-control" name="text"
                        value="{{ $lands->code_indicatif }}" disabled="disabled">
                </div>
            </div>

            <div class="form-group row"><label class="col-lg-2 col-form-label" style=color:blueviolet><center>CodeCapitale:</center></label>
                <div class="col-lg-10"><input type="text" class="form-control" name="nom"
                        value="{{ $lands->capitale}}" disabled="disabled">
                </div>
            </div>
            <div class="form-group row"><label class="col-lg-2 col-form-label" style=color:brown><center>Monnaie:</center></label>

                <div class="col-lg-10"><input type="text" class="form-control" name="nom"
                        value="{{ $lands->monnaie}}" disabled="disabled">
                </div>
            </div>
            <div class="form-group row"><label class="col-lg-2 col-form-label" style=color:darkmagenta><center>Langue:</center></label>

                <div class="col-lg-10"><input type="text" class="form-control" name="nom"
                        value="{{ $lands->Langue }}" disabled="disabled">
                </div>
            </div>
            <div class="form-group row"><label class="col-lg-2 col-form-label" style=color:orangered><center>Superficie:</center></label>

                <div class="col-lg-10"><input type="text" class="form-control" name="nom"
                        value="{{ $lands->Superficie }}" disabled="disabled">
                </div>
            </div>
            <div class="form-group row"><label class="col-lg-2 col-form-label" style=color:lightcoral><center>Est laique:</center></label>

                <div class="col-lg-10"><input type="text" class="form-control" name="nom"
                        value="{{ $lands->est_laique }}" disabled="disabled">
                </div>
            </div>


        </div>
    </div>
</div>

<div class="col-lg-12">
    <div class="ibox ">
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="#" class="dropdown-item">Config option 1</a>
                    </li>
                    <li><a href="#" class="dropdown-item">Config option 2</a>
                    </li>
                </ul>
                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
        <div class="ibox-footer">
            <span class="float-right">
                The righ side of the footer
            </span>
            This is simple footer example
        </div>
    </div>
</div>
</center>

@endsection
