@extends('layouts.main')

@section('content')
<center>
<div>
  <h1 class="pl-5 pr-5" style="color: red"> <center>Liste des pays</center></h1>
  <a class="btn btn-primary btn-rounded" style="margin: 0 0 0 500px" href="{{route('users.create')}}">AJouter Un Pays</a>
</div>

<div class="col-lg-10">
  <div class="ibox ">
      <div class="ibox-title">
          <p><center>Ce tableau met en lumière les informations sur les pays,Welcome!!!!</center> </p>
          <div class="ibox-tools">
              <a class="collapse-link">
                  <i class="fa fa-chevron-up"></i>
              </a>
              <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                  <i class="fa fa-wrench"></i>
              </a>
              <ul class="dropdown-menu dropdown-user">
                  <li><a href="#" class="dropdown-item">Config option 1</a>
                  </li>
                  <li><a href="#" class="dropdown-item">Config option 2</a>
                  </li>
              </ul>
              <a class="close-link">
                  <i class="fa fa-times"></i>
              </a>
          </div>
      </div>
      <div class="ibox-content">

          <table class="table table-hover">
              <thead>
                  <tr style="background-color: green">
                    <th>#</th>
                    <th><center>Libelle</center></th>
                    <th><center>Description</center></th>
                    <th>Code_indicatif</th>
                    <th>Continent</th>
                    <th>Population</th>
                    <th>capitale</th>
                    <th>monnaie</th>
                    <th>Langue</th>
                    <th>Superficie</th>
                    <th>est_laique</th>
                    <th>Actions</th>
                  </tr>
              </thead>
              <tbody>
                  @foreach ($lands as $element)
                      <tr style="background-color:salmon">
                        <td>{{ $element->id}}</td>
                        <td><center>{{ $element->libelle}}</center></td>
                        <td>{{ $element->description}}</td>
                        <td>{{ $element->code_indicatif}}</td>
                        <td>{{ $element->Continent}}</td>
                        <td>{{ $element->Population}}</td>
                        <td>{{ $element->capitale}}</td>
                        <td>{{ $element->monnaie}}</td>
                        <td>{{ $element->Langue}}</td>
                        <td>{{ $element->Superficie}}</td>
                        <td>{{ $element->est_laique}}</td>
                          <td>
                            <a href="{{ route('users.show', ['id' => $element->id]) }}">
                                <button class="btn btn-info btn-circle" type="button">
                                    <i class="fa fa-eye"></i>
                                </button>
                            </a>

                            <a href="{{ route('users.edit', ['id' => $element->id]) }}">
                                <button class="btn btn-success btn-circle" type="button">
                                    <i class="fa fa-pencil"></i>
                                </button>
                            </a>
                             <form method= "POST" action=" route('users.destroy',['id' => $element->id]) }}">
                                @csrf
                               @method("DELETE")
                                <button type="submit" class="btn btn-danger btn-sm" title="Delete lands" onclick="return confirm("Suppression effectuee")">Delete</button>
                            </form>
                        </td>
                      </tr>
                  @endforeach
              </tbody>
          </table>
      </div>
  </div>
</div>
</center>
@endsection
