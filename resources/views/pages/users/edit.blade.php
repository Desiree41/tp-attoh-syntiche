@extends('layouts.main')
@section('content')
<div>
    <h1 class="pl-5 pr-5" style="color:lawngreen"><center>Modification d'un pays<center></h1>
</div>

<div class="col-lg-14">
    <div class="ibox ">
        <div class="ibox-title">
            <p><center>Modifiez,let's goooo<center></p>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-wrench"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="#" class="dropdown-item">Config option 1</a>
                    </li>
                    <li><a href="#" class="dropdown-item">Config option 2</a>
                    </li>
                </ul>
                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
        <!-- Message d'erreur en cas de doublons de code indicatif-->
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
            <form method= "POST" action="route('users.store')}}">
                @csrf 
            @method('POST')
        

                <div class="form-group row"><label class="col-lg-2 col-form-label"> <center>Libelle:</center></label>

                    <div class="col-lg-20"><input type="text" placeholder="Name of country" class="form-control"
                            name="nom" value="{{ $lands->libelle }}">
                    </div>
                </div>
                <div class="form-group row"><label class="col-lg-2 col-form-label"><center>Description:</center></label>
                    <div class="col-lg-20"><input type="text" placeholder="Description"
                            class="form-control" name="Description" value="{{ $lands->description }}">
                    </div>
                </div>

                <div class="form-group row"><label class="col-lg-2 col-form-label"><center>Code Indicatif:</center></label>
                    <div class="col-lg-20"><input type="text" placeholder="+"
                            class="form-control" name="text" value="{{ $lands->code_indicatif }}">
                        </div>
                    </div>
                    <div class="form-group row"><label class="col-lg-2 col-form-label"><center>Continent:</center></label>
                        <div class="col-lg-25"><input type="text" placeholder="Asie"
                                class="form-control" name="text" value="{{ $lands->continent }}">
                            </div>
                        </div>
                        <div class="form-group row"><label class="col-lg-2 col-form-label"><center>Population:</center></label>
                            <div class="col-lg-20"><input type="text" placeholder="Asiatiques"
                                    class="form-control" name="text" value="{{ $lands->Population}}">
                                </div>
                            </div>
                            <div class="form-group row"><label class="col-lg-2 col-form-label"><center>Capitale:</center></label>
                                <div class="col-lg-20"><input type="text" placeholder="Monnaco"
                                        class="form-control" name="text" value="{{ $lands->capitale }}">
                                    </div>
                                </div>
                                <div class="form-group row"><label class="col-lg-2 col-form-label"><center>Monnaie:</center></label>
                                    <div class="col-lg-20"><input type="text" placeholder="Euro"
                                            class="form-control" name="text" value="{{ $lands->monnaie}}">
                                        </div>
                                    </div>
                                    <div class="form-group row"><label class="col-lg-2 col-form-label"><center>Langue:</center></label>
                                        <div class="col-lg-20"><input type="text" placeholder="Engish"
                                                class="form-control" name="text" value="{{ $lands->Langue}}">
                                            </div>
                                        </div>
                                        <div class="form-group row"><label class="col-lg-2 col-form-label"><center>Superficie:</center></label>
                                            <div class="col-lg-20"><input type="text" placeholder="30000km"
                                                    class="form-control" name="text" value="{{ $lands->Superficie }}">
                                                </div>
                                            </div>
                                            <div class="form-group row"><label class="col-lg-2 col-form-label"><center>Statut:</center></label>
                                                <div class="col-lg-20"><input type="text" placeholder="Asie"
                                                        class="form-control" name="text" value="{{ $lands->est_laique }}">
                                                    </div>
                                                </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-offset-2 col-lg-10">
                        <div class="i-checks"><label class="">
                                <div class="icheckbox_square-green" style="position: relative;"><input type="checkbox"
                                        style="position: absolute; opacity: 0;"><ins class="iCheck-helper"
                                        style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                </div>
                            </label></div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-offset-2 col-lg-10">
                        <center><button class="btn btn-sm btn-primary" type="submit"><strong>Modifier</strong></button></center>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection














































































































































































































